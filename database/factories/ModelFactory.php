<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'first_name' => $faker->firstName,
        'middle_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Project\Project::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->title,
        'description' => $faker->sentence,
        'start_date' => $faker->date(),
        'end_date' => $faker->date(),
        'supervisor_id' => 1,
        'member_id' => 1,
        "project_members" => [
            0 => null
        ],
    ];
});