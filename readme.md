## Installation and Configuration of the project

### Live project Access
 [tasksystem.com](http://demistifymathematics.ml/)

### Cloning the project
    git clone https://erickmwazonga@bitbucket.org/erickmwazonga/tasksystem.git
    
### Installing Dependencies
    1. cd into the project root folder
    2. composer install && composer update
    3. npm install 
    4. bower install

### Configuring database
    1. Create database
        mysql -u root -p
        CREATE DATABASE task_system;
        
### Configuring the environment
    1. Create .env file
        cd into the project root folder and touch .env
        
        copy the contents below
        -----------------------------------
           APP_ENV=local
           APP_KEY=base64:HYhyrnqQLMlwEcbdY6J8RI8lBBovWNeno1lqPXGgAvs=
           APP_DEBUG=true
           APP_LOG_LEVEL=debug
           APP_URL=http://localhost
           
           DB_CONNECTION=mysql
           DB_HOST=127.0.0.1
           DB_PORT=3306
           DB_DATABASE=task_system
           DB_USERNAME=your_mysql_username
           DB_PASSWORD=your_mysql_password
           
           BROADCAST_DRIVER=log
           CACHE_DRIVER=file
           SESSION_DRIVER=file
           QUEUE_DRIVER=sync
           
           REDIS_HOST=127.0.0.1
           REDIS_PASSWORD=null
           REDIS_PORT=6379
           
### Synchronizing the Schema
    1. Download the task_system file in the mail attachments
    2. Open your MysqlClinet -e.g Mysql Workbench, Sqlpro etc
    4. Open the downloaded schema file in the mysql-client
    5. According to your client, go to settings e,g in Mysql Workbench go to Settings->Synchronize Model and follow the steps
   
### Run the project
#### 1. Through php artisan
        cd into the project root folder
        php artisan serve
        visit 127.0.0.1:8000
        
#### 1. Through Reverse Proxy such as NGINX
[Follow these Instructions](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04)