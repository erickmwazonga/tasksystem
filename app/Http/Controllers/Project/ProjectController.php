<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectRequest;
use App\Project\Project;
use App\Project\ProjectMember;
use App\User;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;
use function PHPSTORM_META\map;

class ProjectController extends Controller
{
    public function create()
    {
        return view('project.create_project');
    }

    public function save(ProjectRequest $request)
    {
        $input = $request->all();

        $project_members = json_decode($input['project_members'][0]);

        if ($input['supervisor_id'] == 0) {
            Flash::error('Kindly ensure you select project supervisor');

            return redirect()->back()->withInput();
        }

        if (count($project_members) == null || count($project_members) == 0) {
            Flash::error('Kindly ensure you select project members');

            return redirect()->back()->withInput();
        } else {
            $project = new Project();

            $project->name = $input['name'];
            $project->description = $input['description'];
            $project->start_date = $input['start_date'];
            $project->end_date = $input['end_date'];
            $project->supervisor_id = $input['supervisor_id'];
            $project->created_by = Auth::user()->id;

            $project->save();

            //Save project members
            foreach ($project_members as $input_project_member) {
                $project_member = new ProjectMember();

                $project_member->project_id = $project->id;
                $project_member->user_id = $input_project_member->member_id;

                $project_member->save();
            }

            Flash::success('Project Successfully Created!');
            return redirect(route('home'));
        }
    }

    public function edit($project_id)
    {
        $project = Project::findOrFail($project_id);

        return (view('project.edit_project', compact('project')));
    }

    public function update(ProjectRequest $request, $project_id)
    {
        $input = $request->all();

        $project_members = json_decode($input['project_members'][0]);


        if ($input['supervisor_id'] == 0) {
            Flash::error('Kindly ensure you select the project supervisor');

            return redirect()->back()->withInput();
        }

        $project = Project::findOrFail($project_id);

        $project->name = $input['name'];
        $project->description = $input['description'];
        $project->start_date = $input['start_date'];
        $project->end_date = $input['end_date'];
        $project->supervisor_id = $input['supervisor_id'];
        $project->created_by = Auth::user()->id;

        $project->save();

        $current_project_members = ProjectMember::where('project_id', $project->id);

        if ($project_members == null && $input['project_member_removed'] == 1) {
            Flash::error('Kindly add the project members');

            return redirect()->back()->withInput();
        }

        if ($project_members != null) {

            $current_project_members->delete();

            //Save project members
            foreach ($project_members as $input_project_member) {
                $project_member = new ProjectMember();

                $project_member->project_id = $project->id;
                $project_member->user_id = $input_project_member->id;

                $project_member->save();
            }
        }

        Flash::success('Project Successfully Edited!');

        return redirect()->route('home');
    }

    public function display($project_id)
    {
        $project = Project::findOrFail($project_id);

        return view('project.view_project', compact('project'));
    }

    public function getUsers()
    {
        $users = User::all()->toArray();

        $users = collect($users)->map(function ($user) {
            return ['id' => $user['id'], 'name' => $user['first_name'] . ' ' . $user['last_name']];
        });

        return $users;
    }

    public function getMembers($project_id)
    {
        $project_members = ProjectMember::where('project_id', $project_id)->get();

        if ($project_members) {
            $project_members = collect($project_members)->map(function ($project_member) {
                return ['id' => $project_member->user_id, 'name' => getUserFullName($project_member->user_id)];
            });

            return $project_members;
        }

        return $project_members;
    }
}
