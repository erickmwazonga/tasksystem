<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use App\Http\Requests\TaskRequest;
use App\Task\Task;
use App\Task\TaskMember;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Laracasts\Flash\Flash;


class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all();

        $pending_tasks = Task::where('status', 0)->get();

        $ongoing_tasks = Task::where('status', 1)->get();

        $under_review_tasks = Task::where('status', 2)->get();

        $done_tasks = Task::where('status', 3)->get();

        return view('task.view_tasks', compact('tasks', 'pending_tasks', 'ongoing_tasks', 'under_review_tasks', 'done_tasks'));
    }

    public  function create()
    {
        return view('task.create_task');
    }

    public function save(TaskRequest $request)
    {
        $input = $request->all();

        $task_members = json_decode($input['project_members'][0]);

        //Check if there is a file attachment
        if (count($input) <= 13) {
            $input['task_attachment'] = null;
        }

        if ($input['project_id'] == 0) {
            Flash::error('Kindly ensure you select the project that the task belongs to');

            return redirect()->back()->withInput();
        }

        if ($task_members == null) {
            Flash::error('Kindly ensure you add members to the task');

            return redirect()->back()->withInput();
        } else {
            $task = new Task();

            $task->name = $input['name'];
            $task->description = $input['description'];
            $task->project_id = $input['project_id'];
            $task->created_by = Auth::user()->id;
            $task->access_level = $input['access_level'];
            $task->status = $input['status'];
            $task->priority = $input['priority'];
            $task->start_date = $input['start_date'];
            $task->end_date = $input['end_date'];
            $task->task_category_id = $input['task_category_id'];

            if ($input['task_attachment'] != null) {
                $task_attachment_name = $task->name . '.' . $input['task_attachment']->getClientOriginalExtension();

                $task->task_attachment = $task_attachment_name;

                $input['task_attachment']->move(
                    storage_path() . '/task_attachments/'. $task_attachment_name
                );
            }

            $task->save();

            foreach ($task_members as $input_task_member) {
                $task_member = new TaskMember();

                $task_member->task_id = $task->id;
                $task_member->project_id = $task->project_id;
                $task_member->user_id = $input_task_member->member_id;

                $task_member->save();
            }

            Flash::success('Task Successfully Created!');

            return redirect('/tasks');
        }
    }

    public function delete($task_id)
    {
        $task = Task::findOrFail($task_id);

        $task_members = $task->members;

        if(!$task_members) {
            $task->delete();

            Flash::success('Task Successfully Deleted!');

            return redirect()->back();

        }else {
            Flash::error('Task with members cannot be deleted');

            return redirect()->back();
        }
    }

    public function edit($task_id)
    {
        $task = Task::findOrFail($task_id);

        return view('task.edit_task', compact('task'));
    }

    public function update(TaskRequest $request, $task_id)
    {
        $input = $request->all();

        $task_members = json_decode($input['project_members'][0]);

        //Check if there is a file attachment
        if (count($input) <= 14) {
            $input['task_attachment'] = null;
        }

        if ($input['project_id'] == 0) {
            Flash::error('Kindly ensure you select the project that the task belongs to');

            return redirect()->back()->withInput();
        }

        $task = Task::findOrFail($task_id);

        $task->name = $input['name'];
        $task->description = $input['description'];
        $task->project_id = $input['project_id'];
        $task->created_by = Auth::user()->id;
        $task->access_level = $input['access_level'];
        $task->status = $input['status'];
        $task->priority = $input['priority'];
        $task->start_date = $input['start_date'];
        $task->end_date = $input['end_date'];
        $task->task_category_id = $input['task_category_id'];

        if ($input['task_attachment'] != null) {
            $task_attachment_name = $task->name . '.' . $input['task_attachment']->getClientOriginalExtension();

            $task->task_attachment = $task_attachment_name;

            $input['task_attachment']->move(
                storage_path() . '/task_attachments/'. $task_attachment_name
            );
        }

        $task->save();

        $current_task_members = TaskMember::where('task_id', $task->id);

        if ($task_members == null && $input['project_member_removed'] == 1) {
            Flash::error('Kindly add members to this task!');

            return redirect()->back()->withInput();
        }

        if ($task_members != null) {

            $current_task_members->delete();

            foreach ($task_members as $input_task_member) {
                $task_member = new TaskMember();

                $task_member->project_id = $task->project_id;
                $task_member->user_id = $input_task_member->id;
                $task_member->task_id = $task->id;

                $task_member->save();
            }
        }

        Flash::success('Task Successfully Edited!');

        return redirect('/tasks');
    }

    public function display($task_id)
    {
        $task = Task::findOrFail($task_id);

        return view('task.display_task_details', compact('task'));
    }

    public function getTaskMembers($task_id)
    {
        $task_members = TaskMember::where('task_id', $task_id)->get();

        if ($task_members) {
            $task_members = collect($task_members)->map(function ($task_member) {
                return ['id' => $task_member->user_id, 'name' => getUserFullName($task_member->user_id)];
            });

            return $task_members;
        }

        return $task_members;
    }

    public function send(Request $request){
        $title = $request->input('title');
        $content = $request->input('content');

        Mail::send('emails.task_send', ['title' => $title, 'content' => $content], function ($message)
        {

            $message->from('erickmwazonga@gmail', 'Rick and Morty');

            $message->to('fe@gmail.com');

        });

        return response()->json(['message' => 'Request completed']);
    }
}