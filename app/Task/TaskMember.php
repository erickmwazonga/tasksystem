<?php

namespace App\Task;

use Illuminate\Database\Eloquent\Model;

class TaskMember extends Model
{
    protected $table = 'task_members';

    protected $guarded = ['id', 'created_at', 'updated_at'];
}
