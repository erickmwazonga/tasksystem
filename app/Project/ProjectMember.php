<?php

namespace App\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectMember extends Model
{
    protected $table = 'project_members';

    protected $guarded = ['id', 'created_at', 'updated_at'];
}
