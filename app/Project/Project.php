<?php

namespace App\Project;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public  function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public  function supervisor()
    {
        return $this->belongsTo(User::class, 'supervisor_id');
    }

    public function members()
    {
        return $this->hasMany(ProjectMember::class, 'project_id');
    }

}
