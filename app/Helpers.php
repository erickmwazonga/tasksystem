<?php
/**
 * Created by PhpStorm.
 * User: erick
 * Date: 5/16/18
 * Time: 3:06 AM
 */

function projects_array()
{
    return array_prepend(\App\Project\Project::pluck('name', 'id')->toArray(), 'Select Project', 0);
}

function categories_array()
{
    return [
        0 => 'Design',
        1 => 'Development',
        2 => 'Testing',
        3 => 'Deployment'
    ];
}

function access_levels_array()
{
    return [
        0 => 'Public',
        1 => 'Private',
    ];
}

function task_status_array()
{
    return [
        0 => 'Pending',
        1 => 'Ongoing',
        2 => 'Under Review',
        3 => 'Done'
    ];
}

function task_priority_array()
{
    return [
        0 => 'High',
        1 => 'Medium',
        2 => 'Low',
    ];
}

function users_array()
{
    $users = \App\User::all();
    $users_array = [];

    foreach ($users as $user) {
        $users_array[$user->id] = $user->first_name .' '. $user->last_name;
    }

    return array_prepend($users_array, 'Select User');
}

function getUserFullName($user_id)
{
    $user = \App\User::find($user_id);

    if (!$user) {
        return null;
    }

    return $user->first_name .' '. $user->last_name;
}
