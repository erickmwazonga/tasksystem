@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="large-12 columns">
                <div class="text-center">
                    <h4>Projects</h4>
                </div>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="large-12 columns">
                <a href="/projects/create" class="button pull-right">Create Project</a>
            </div>
        </div>


        <div class="row">
            @foreach($projects as $index => $project)
                <div class="large-3 medium-6 small-12 columns">
                    <div class="project">
                        <div class="project-header">
                            {!! ++$index  !!}.{!! $project->name !!}
                        </div>

                        <div class="project-content">
                            {!! $project->description !!}
                        </div>

                        <div class="project-timeline">
                            <hr>
                            Start Date: {!! $project->start_date !!}
                            <br>
                            End Date: {!! $project->end_date !!}
                        </div>

                        <div class="project-footer">
                            <a href="/projects/view/{!! $project->id !!}"><span class="fa fa-eye"></span></a> &nbsp;
                            <a href="/projects/edit/{!! $project->id !!}"><span class="fa fa-edit"></span></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
