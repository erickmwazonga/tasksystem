@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="medium-8 medium-offset-2 columns">
            <div class="project">
                <div class="project-header">
                    Project Details
                </div>

                <div class="project-content">
                    Project Name: <b>{!! $project->name !!}</b>
                    <br>
                    Project Description: <b>{!! $project->description !!}</b>
                    <br>
                    Supervisor: <b>{!! getUserFullName($project->supervisor_id) !!}</b>
                    <br>
                    Created By: <b>{!! getUserFullName($project->created_by) !!}</b>

                    <hr>
                    Members:
                    <br>
                    @if(count($project->members) != 0)
                        @foreach($project->members as $project_member)
                            <span class="label add-member">
                                {!! getUserFullName($project_member->user_id) !!}
                            </span>
                            <input type="hidden" ng-value="project_members" name="project_members[]">
                        @endforeach
                    @endif
                </div>

                <div class="project-timeline">
                    <hr>
                    Start Date: {!! $project->start_date !!}
                    <br>
                    End Date: {!! $project->end_date !!}
                </div>

                <div class="project-footer">
                    <a href="/home" class="button">Back</a> &nbsp;&nbsp;
                    <a href="/projects/edit/{!! $project->id !!}" class="button secondary">Edit</a>
                </div>
            </div>
        </div>
    </div>

@endsection