@extends('layouts.app')

@section('content')
    <script>
        $(function () {
            $('#datePickerStart').fdatepicker({
                format: 'yyyy-mm-dd',
                disableDblClickSelection: true,
            })
        });

        $(function () {
            $('#datePickerEnd').fdatepicker({
                format: 'yyyy-mm-dd',
                disableDblClickSelection: true,
            })
        })
    </script>

    @extends('layouts.app')

@section('content')
    <script>
        $(function () {
            $('#datePickerStart').fdatepicker({
                format: 'yyyy-mm-dd',
                disableDblClickSelection: true,
            })
        });

        $(function () {
            $('#datePickerEnd').fdatepicker({
                format: 'yyyy-mm-dd',
                disableDblClickSelection: true,
            })
        })
    </script>

    <div class="row">
        <div class="medium-8 medium-offset-2 columns" ng-controller="TestController">
            {!! Form::open(['route' => 'project-save']) !!}
            <div class="project">
                <div class="project-header">
                    Create Project
                </div>

                <div class="project-content-edit">
                    <div class="row">
                        {!! Form::label('Name') !!}
                        {!! Form::text('name', null, ['class' => 'form-control', 'place_holder' => 'Enter Project Name', 'required']) !!}

                        {!! Form::label('Description') !!}
                        {!! Form::text('description', null, ['class' => 'form-control', 'place_holder' => 'Enter Description', 'required']) !!}


                        {!! Form::label('Start Date') !!}
                        {!! Form::text('start_date', null, ['class' => 'form-control', 'place_holder' => 'Enter Project Name', 'id' => 'datePickerStart', 'required']) !!}

                        {!! Form::label('End Date') !!}
                        {!! Form::text('end_date', null, ['class' => 'form-control', 'place_holder' => 'Enter Project Name', 'id' => 'datePickerEnd',  'required']) !!}

                        <div class="row">
                            <hr>
                        </div>
                        <br>

                        {!! Form::label('Project Supervisor') !!}
                        {!! Form::select('supervisor_id', users_array(), ['class' => 'form-control', 'required']) !!}

                        {!! Form::label('Project Members') !!}

                        <select name="member_id"
                                ng-model="memberSelected"
                                data-ng-options="member as member.name for member in members">
                            <option value="">Select Project Members</option>
                        </select>

                        <div>
                            <a href="" ng-click="addProjectMember(memberSelected)"><span class="fa fa-plus-circle">Add Member</span></a>
                        </div>

                        <br>

                        <div class="large-12 columns">
                            <span class="label add-member"
                                  ng-if="project_members_array.length != 0"
                                  ng-repeat="member in project_members_array">
                                 <%member.name%>
                                <i class="fa fa-times" aria-hidden="true"  ng-click="removeProjectMember(project_members_array, member)"></i>
                            </span>
                            <input type="hidden" ng-value="project_members" name="project_members[]">
                        </div>
                    </div>
                </div>

                <div class="project-footer">
                    {!! Form::submit('Submit', ['class' => 'button']) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection