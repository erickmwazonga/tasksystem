@extends('layouts.app')

@section('content')
    <div ng-app="app">
        <div class="container">
            <div class="row">
                <div class="large-12 columns">
                    <div class="text-center">
                        <h4>Tasks</h4>
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="large-12 columns">
                    <a href="/tasks/create" class="button pull-right">Create Task</a>
                </div>
            </div>


            <div class="row">
                <div class="large-6 medium-6 small-12 columns">
                    <div class="project">
                        <div class="project-header">
                            Pending Tasks
                        </div>
                        <div class="project-content">
                            <div class="table-container">
                                <table class="unstriped">
                                    <tbody>
                                    @foreach($pending_tasks as $index => $task)
                                        <tr>
                                            <td>
                                                {!! ++$index  !!}
                                            </td>
                                            <td>
                                                {{ str_limit($task->name , $limit = 35, $end = '...') }}
                                            </td>
                                            <td>
                                                    <span class="label success">
                                                        @if($task->access_level == 0)
                                                            Public
                                                        @else
                                                            Private
                                                        @endif
                                                    </span>
                                            </td>
                                            <td>
                                                    <span class="label primary">
                                                        @if($task->status == 0)
                                                            Pending
                                                        @elseif($task->status == 1)
                                                            Ongoing
                                                        @elseif($task->status == 2)
                                                            Under Review
                                                        @else
                                                            Done
                                                        @endif
                                                    </span>
                                            </td>
                                            <td>
                                                    <span class="label secondary">
                                                        {!! $task->end_date !!}
                                                    </span>
                                            </td>
                                            <td>
                                                <a href="/tasks/view/{!! $task->id !!}"><span class="fa fa-eye"></span></a>
                                                &nbsp;
                                                <a href="/tasks/edit/{!! $task->id !!}"><span class="fa fa-edit"></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="large-6 medium-6 small-12 columns">
                    <div class="project">
                        <div class="project-header">
                            Ongoing Tasks
                        </div>
                        <div class="project-content">
                            <div class="table-container">
                                <table class="unstriped">
                                    <tbody>
                                    @foreach($ongoing_tasks as $index => $task)
                                        <tr>
                                            <td>
                                                {!! ++$index  !!}
                                            </td>
                                            <td>
                                                {{ str_limit($task->name , $limit = 35, $end = '...') }}
                                            </td>
                                            <td>
                                                    <span class="label success">
                                                        @if($task->access_level == 0)
                                                            Public
                                                        @else
                                                            Private
                                                        @endif
                                                    </span>
                                            </td>
                                            <td>
                                                    <span class="label primary">
                                                        @if($task->status == 0)
                                                            Pending
                                                        @elseif($task->status == 1)
                                                            Ongoing
                                                        @elseif($task->status == 2)
                                                            Under Review
                                                        @else
                                                            Done
                                                        @endif
                                                    </span>
                                            </td>
                                            <td>
                                                    <span class="label secondary">
                                                        {!! $task->end_date !!}
                                                    </span>
                                            </td>
                                            <td>
                                                <a href="/tasks/view/{!! $task->id !!}"><span class="fa fa-eye"></span></a>
                                                &nbsp;
                                                <a href="/tasks/edit/{!! $task->id !!}"><span class="fa fa-edit"></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="large-6 medium-6 small-12 columns">
                    <div class="project">
                        <div class="project-header">
                            Under Review Tasks
                        </div>
                        <div class="project-content">
                            <div class="table-container">
                                <table class="unstriped">
                                    <tbody>
                                    @foreach($under_review_tasks as $index => $task)
                                        <tr>
                                            <td>
                                                {!! ++$index  !!}
                                            </td>
                                            <td>
                                                {{ str_limit($task->name , $limit = 35, $end = '...') }}
                                            </td>
                                            <td>
                                                    <span class="label success">
                                                        @if($task->access_level == 0)
                                                            Public
                                                        @else
                                                            Private
                                                        @endif
                                                    </span>
                                            </td>
                                            <td>
                                                    <span class="label primary">
                                                        @if($task->status == 0)
                                                            Pending
                                                        @elseif($task->status == 1)
                                                            Ongoing
                                                        @elseif($task->status == 2)
                                                            Under Review
                                                        @else
                                                            Done
                                                        @endif
                                                    </span>
                                            </td>
                                            <td>
                                                    <span class="label secondary">
                                                        {!! $task->end_date !!}
                                                    </span>
                                            </td>
                                            <td>
                                                <a href="/tasks/view/{!! $task->id !!}"><span class="fa fa-eye"></span></a>
                                                &nbsp;
                                                <a href="/tasks/edit/{!! $task->id !!}"><span class="fa fa-edit"></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="large-6 medium-6 small-12 columns">
                    <div class="project">
                        <div class="project-header">
                            Done Tasks
                        </div>
                        <div class="project-content">
                            <div class="table-container">
                                <table class="unstriped">
                                    <tbody>
                                    @foreach($done_tasks as $index => $task)
                                        <tr>
                                            <td>
                                                {!! ++$index  !!}
                                            </td>
                                            <td>
                                                {{ str_limit($task->name , $limit = 35, $end = '...') }}
                                            </td>
                                            <td>
                                                    <span class="label success">
                                                        @if($task->access_level == 0)
                                                            Public
                                                        @else
                                                            Private
                                                        @endif
                                                    </span>
                                            </td>
                                            <td>
                                                    <span class="label primary">
                                                        @if($task->status == 0)
                                                            Pending
                                                        @elseif($task->status == 1)
                                                            Ongoing
                                                        @elseif($task->status == 2)
                                                            Under Review
                                                        @else
                                                            Done
                                                        @endif
                                                    </span>
                                            </td>
                                            <td>
                                                    <span class="label secondary">
                                                        {!! $task->end_date !!}
                                                    </span>
                                            </td>
                                            <td>
                                                <a href="/tasks/view/{!! $task->id !!}"><span class="fa fa-eye"></span></a>
                                                &nbsp;
                                                <a href="/tasks/edit/{!! $task->id !!}"><span class="fa fa-edit"></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection