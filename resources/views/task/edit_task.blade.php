@extends('layouts.app')

@section('content')
    <script>
        $(function () {
            $('#datePickerStart').fdatepicker({
                format: 'yyyy-mm-dd',
                disableDblClickSelection: true,
            })
        });

        $(function () {
            $('#datePickerEnd').fdatepicker({
                format: 'yyyy-mm-dd',
                disableDblClickSelection: true,
            })
        })
    </script>

    @if(count($errors) > 0)
        <div data-closable class="callout warning radius">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row" ng-controller="EditTaskController"  ng-init="taskId = '{!! $task->id !!}'">
        <div class="medium-8 medium-offset-2 columns">
            {!! Form::open(['url' => route('task-update', $task->id), 'files' => true]) !!}
            <div class="task">
                <div class="task-header">
                    Create Task
                </div>

                <div class="task-content-edit">
                    <div class="row">
                        {!! Form::label('Name') !!}
                        {!! Form::text('name', $task->name, ['class' => 'form-control', 'placeholder' => 'Enter Task Name', 'required']) !!}

                        {!! Form::label('Description') !!}
                        {!! Form::text('description', $task->description, ['class' => 'form-control', 'placeholder' => 'Enter Description', 'required']) !!}

                        {!! Form::label('project_id', 'Project') !!}
                        {!! Form::select('project_id', projects_array(), $task->project_id, ['class' => 'form-control', 'required']) !!}

                        {!! Form::label('Task Category') !!}
                        {!! Form::select('task_category_id',categories_array(), $task->task_category_id, ['class' => 'form-control', 'required']) !!}

                        {!! Form::label('Access Level') !!}
                        {!! Form::select('access_level', access_levels_array(), $task->access_level,  ['class' => 'form-control', 'required']) !!}

                        {!! Form::label('Status') !!}
                        {!! Form::select('status', task_status_array(), $task->status, ['class' => 'form-control', 'required']) !!}

                        {!! Form::label('Priority') !!}
                        {!! Form::select('priority', task_priority_array(), $task->priority, ['class' => 'form-control', 'required']) !!}

                        {!! Form::label('Start Date') !!}
                        {!! Form::text('start_date', $task->start_date, ['class' => 'form-control', 'placeholder' => 'Enter Start Date', 'id' => 'datePickerStart', 'required']) !!}

                        {!! Form::label('End Date') !!}
                        {!! Form::text('end_date', $task->end_date, ['class' => 'form-control', 'placeholder' => 'Enter End Date', 'id' => 'datePickerEnd',  'required']) !!}

                        {!! Form::label('Attach File(e.g Task Documentation)') !!}
                        {!! Form::file('task_attachment', null) !!}

                        {!! Form::label('Task Members') !!}

                        <select name="member_id"
                                ng-model="memberSelected"
                                data-ng-options="member as member.name for member in members">
                            <option value="">Select Task Members</option>
                        </select>

                        <div>
                            <a href="" ng-click="editProjectMember(memberSelected)"><span class="fa fa-plus-circle">Add Member to Task</span></a>
                        </div>

                        <div class="large-12 columns">
                            <span class="label add-member"
                                  ng-if="project_members_edit_array.length != 0"
                                  ng-repeat="member in project_members_edit_array">
                                 <%member.name%>
                                <i class="fa fa-times" aria-hidden="true" ng-click="removeEditedProjectMember(project_members_edit_array, member)"></i>
                            </span>
                            <input type="hidden" ng-value="project_members" name="project_members[]">
                            <input type="hidden" ng-value="project_member_removed" name="project_member_removed">
                        </div>
                    </div>
                </div>

                <div class="task-footer">
                    {!! Form::submit('Submit', ['class' => 'button']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
