@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="medium-8 medium-offset-2 columns">
            <div class="project">
                <div class="project-header">
                    Task Details
                </div>

                <div class="project-content">
                    Task Name: <b>{!! $task->name !!}</b>
                    <br>
                    Task Description: <b>{!! $task->description !!}</b>
                    <br>
                    Project: <b>{!! $task->project->name !!}</b>
                    <br>
                    Created By: <b>{!! getUserFullName($task->created_by) !!}</b>

                    <hr>
                    Members:
                    <br>
                    @if(count($task->members) != 0)
                        @foreach($task->members as $task_member)
                            <span class="label add-member">
                                {!! getUserFullName($task_member->user_id) !!}
                            </span>
                        @endforeach
                    @endif
                </div>

                <div class="project-timeline">
                    <hr>
                    Start Date: {!! $task->start_date !!}
                    <br>
                    End Date: {!! $task->end_date !!}
                </div>

                <div class="project-footer">
                    <a href="/tasks" class="button">Back</a> &nbsp;&nbsp;
                    <a href="/tasks/edit/{!! $task->id !!}" class="button secondary">Edit</a>
                </div>
            </div>
        </div>
    </div>

@endsection