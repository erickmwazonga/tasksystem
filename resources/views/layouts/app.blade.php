<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" ng-app="app">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'The Task System') }}</title>

    {{--<script src="/js/angular.min.js"></script>--}}
    <script src="{{ mix('js/app.js') }}"></script>
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/foundation-datepicker.css">
    <link href="https://fonts.googleapis.com/css?family=Bellefair" rel="stylesheet">

    <script src="/js/foundation-datepicker.js"></script>

    <link rel="stylesheet" href="/bower_components/angular-ui-notification/dist/angular-ui-notification.min.css">
    <script src="/bower_components/angular-ui-notification/dist/angular-ui-notification.min.js"></script>

</head>

<body>
<div class="app-dashboard">
    <div class="row expanded app-dashboard-top-nav-bar">
        <div class="columns medium-2">
            <button data-toggle="app-dashboard-sidebar" class="menu-icon hide-for-medium"></button>
            <a class="app-dashboard-logo">The Task System</a>
        </div>
    </div>

    <div class="app-dashboard-body off-canvas-wrapper">
        <div id="app-dashboard-sidebar"
             class="app-dashboard-sidebar position-left off-canvas off-canvas-absolute reveal-for-medium"
             data-off-canvas>
            <div class="app-dashboard-sidebar-title-area">
                <div class="app-dashboard-close-sidebar">
                    <h3 class="app-dashboard-sidebar-block-title">Dashboard</h3>
                    <button id="close-sidebar" data-app-dashboard-toggle-shrink
                            class="app-dashboard-sidebar-close-button show-for-medium" aria-label="Close menu"
                            type="button">
                        <span aria-hidden="true"><a href="#"><i class="large fa fa-bars"></i></a></span>
                    </button>
                </div>
                <div class="app-dashboard-open-sidebar">
                    <button id="open-sidebar" data-app-dashboard-toggle-shrink
                            class="app-dashboard-open-sidebar-button show-for-medium" aria-label="open menu"
                            type="button">
                        <span aria-hidden="true"><a href="#"><i class="large fa fa-angle-double-right"></i></a></span>
                    </button>
                </div>
            </div>
            <div class="app-dashboard-sidebar-inner">
                <ul class="menu vertical">
                    <li>
                        <a href="/home" class="active">
                            <i class="large fa fa-institution"></i>
                            <span class="app-dashboard-sidebar-text">Projects</span>
                        </a>
                    </li>
                    <li>
                        <a href="/tasks">
                            <i class="large fa fa-tasks"></i>
                            <span class="app-dashboard-sidebar-text">My Tasks</span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <i class="large fa fa-cogs"></i>
                            <span class="app-dashboard-sidebar-text">Settings</span>
                        </a>
                    </li>
                    @if (Auth::guest())
                        <li>
                            <a href="{{ route('login') }}">
                                <i class="large fa fa-sign-in"></i>
                                <span class="app-dashboard-sidebar-text">Login</span>
                            </a>
                        </li>
                    @else
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();" style="color: red;">
                                <i class="large fa fa-sign-out"></i>
                                <span class="app-dashboard-sidebar-text">Logout</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endif
                </ul>
            </div>
        </div>

        <div class="app-dashboard-body-content off-canvas-content" data-off-canvas-content>
            <div>
                @if (Session::has('flash_notification.message'))
                    <div data-closable class="callout {{ Session::get('flash_notification.level') }} radius">
                        {{ Session::get('flash_notification.message') }}
                        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>

            <div class="container main-content">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<script> $(document).foundation();</script>

<script src="/bower_components/angular-route/angular-route.min.js"></script>

</body>
</html>
