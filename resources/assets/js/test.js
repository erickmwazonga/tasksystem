(function () {

    'use strict';

    var app = angular.module('app', ['ngRoute', 'ui-notification']);

    app.config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });

    app.config(function (NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 7000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'center',
            positionY: 'top'
        });
    });

    app.controller('TestController', ['$scope', '$http', 'Notification', function ($scope, $http, Notification) {
        $scope.project_members_array = [];
        $scope.project_members = '';
        $scope.members = '';
        $scope.project_members_edit_array = [];

        $http.get("/api/all-users")
            .then(function (response) {
                $scope.members = response.data;
            });


        $scope.addProjectMember = function (memberSelected) {
            if (memberSelected !== null && memberSelected !== '' && memberSelected !== undefined) {
                $scope.project_members_array.push({
                    member_id: memberSelected.id,
                    name: memberSelected.name
                });

                $scope.project_members = angular.toJson($scope.project_members_array, true);
            } else {
                Notification.error('Kindly select the employee to be added!');
            }
        };
        
        $scope.removeProjectMember = function (project_members_array, member) {
            let index = project_members_array.indexOf(member);

            project_members_array.splice(index, 1);

            $scope.project_members = angular.toJson($scope.project_members_array);

            Notification.success('Successfully removed the member from the project');
        }
    }
    ]);

    app.controller('EditProjectController', ['$scope', '$http', 'Notification', function ($scope, $http, Notification) {
        $scope.project_members = '';
        $scope.members = '';
        $scope.project_members_edit_array = [];
        $scope.projectId = '';
        $scope.project_member_removed = 0;

        $http.get("/api/all-users")
            .then(function (response) {
                $scope.members = response.data;
            });

        $scope.$watch(function () {
            return $scope.projectId;
        },
            function (oldValue, newValue) {
                if (newValue) {
                    $scope.projectId = newValue;
                }

                $http.get('/api/get-project-members/'+ $scope.projectId)
                    .then(function (response) {
                        $scope.project_members_edit_array = response.data;

                        $scope.editProjectMember = function (memberSelected) {
                            if (memberSelected !== null && memberSelected !== '' && memberSelected !== undefined) {
                                $scope.project_members_edit_array.push({
                                    id: memberSelected.id,
                                    name: memberSelected.name
                                });

                                $scope.project_members = angular.toJson($scope.project_members_edit_array, true);

                            } else {
                                Notification.error('Kindly select the employee to be added!');
                            }
                        };

                        $scope.removeEditedProjectMember = function (project_members_edit_array, member) {
                            let index = project_members_edit_array.indexOf(member);

                            project_members_edit_array.splice(index, 1);

                            $scope.project_members = angular.toJson($scope.project_members_edit_array);

                            $scope.project_member_removed = 1;

                            Notification.success('Successfully removed the member from the project');
                        }
                    });
            })
    }]);

    app.controller('EditTaskController', ['$scope', '$http', 'Notification', function ($scope, $http, Notification) {
        $scope.project_members = '';
        $scope.members = '';
        $scope.project_members_edit_array = [];
        $scope.taskId = '';
        $scope.project_member_removed = 0;

        $http.get("/api/all-users")
            .then(function (response) {
                $scope.members = response.data;
            });

        $scope.$watch(function () {
                return $scope.taskId;
            },
            function (oldValue, newValue) {
                if (newValue) {
                    $scope.taskId = newValue;
                }

                $http.get('/api/get-task-members/'+ $scope.taskId)
                    .then(function (response) {
                        $scope.project_members_edit_array = response.data;

                        $scope.editProjectMember = function (memberSelected) {
                            if (memberSelected !== null && memberSelected !== '' && memberSelected !== undefined) {
                                $scope.project_members_edit_array.push({
                                    id: memberSelected.id,
                                    name: memberSelected.name
                                });

                                $scope.project_members = angular.toJson($scope.project_members_edit_array, true);

                            } else {
                                Notification.error('Kindly select the employee to be added!');
                            }
                        };

                        $scope.removeEditedProjectMember = function (project_members_edit_array, member) {
                            let index = project_members_edit_array.indexOf(member);

                            project_members_edit_array.splice(index, 1);

                            $scope.project_members = angular.toJson($scope.project_members_edit_array);

                            $scope.project_member_removed = 1;

                            Notification.success('Successfully removed the member from the project');
                        }
                    });
            })
    }]);


})();