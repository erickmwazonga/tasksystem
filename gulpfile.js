var elixir = require('laravel-elixir');
var paths = {
    'foundation': './node_modules/foundation-sites/',
    'jquery':'./bower_components/jquery/dist/',
    'fastclick': './bower_components/fastclick/',
    'jquery.cookie': './bower_components/jquery.cookie',
    'jquery-placeholder': './bower_components/jquery-placeholder',
    'modernizr': './bower_components/modernizer',
};

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass('app.scss','public/css/app.css')
        .scripts(['/node_modules/foundation-sites/dist/js/foundation.min.js', '/node_modules/jquery/dist/jquery.min.js', 'task_system.js'], 'public/js/main.js')
        .version(["css/app.css", "js/main.js"])
        .copy(paths.jquery + 'jquery.min.js', 'public/js/jquery.min.js')
        .copy(paths.foundation + 'js**', 'public/js/foundation')
        .copy(paths.foundation + 'dist/foundation.js', 'public/js/foundation/foundation.js')
        .copy('node_modules/font-awesome/fonts', 'public/fonts');
});

