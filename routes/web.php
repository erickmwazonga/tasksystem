<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/projects/create', 'Project\ProjectController@create')->name('project-create');

Route::post('/projects/save', 'Project\ProjectController@save')->name('project-save');

Route::get('/projects/edit/{id}', 'Project\ProjectController@edit')->name('project-edit');

Route::post('/projects/update/{id}', 'Project\ProjectController@update')->name('project-update');

Route::get('/projects/view/{id}', 'Project\ProjectController@display')->name('project-view');

Route::get('/tasks', 'Task\TaskController@index')->name('tasks-view');

Route::get('/tasks/create', 'Task\TaskController@create')->name('task-create');

Route::post('/tasks/save', 'Task\TaskController@save')->name('task-save');

Route::get('/tasks/edit/{task_id}', 'Task\TaskController@edit')->name('task-edit');

Route::post('/tasks/update/{task_id}', 'Task\TaskController@update')->name('task-update');

Route::get('/tasks/view/{task_id}', 'Task\TaskController@display')->name('task-display');

Route::get('/tasks/delete/{task_id}', 'Task\TaskController@delete')->name('task-delete');

Route::post('/tasks/send', 'Task\TaskController@send')->name('task-send');

Route::get('/api/all-users', 'Project\ProjectController@getUsers');

Route::get('/api/get-project-members/{project_id}', 'Project\ProjectController@getMembers');

Route::get('/api/get-task-members/{task_id}', 'Task\TaskController@getTaskMembers');

