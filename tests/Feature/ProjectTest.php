<?php

namespace Tests\Feature;

use App\Project\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProjectTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /** @test */
    public function can_access_the_projects_page_after_signing_up()
    {
        $user = factory(User::class)->create();

        $this->post(route('register'), $user->toArray())->assertRedirect('/');

        $this->assertDatabaseHas('users', $user->toArray());
    }

    /** @test */
    public function can_create_a_project()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        Session::start();

        $data = [
            'name' => 'Test Project',
            'description' => 'Test Project Description',
            'start_date' => Carbon::now(),
            'end_date' => Carbon::now()->addMonths(3),
            'supervisor_id' => 1,
            'member_id' => 1,
            "project_members" => ["[{\"member_id\": 2,\r\n
          \"name\": \"dee on\"\r\n}]"],];

        $this->post(route('project-save'), $data)->assertRedirect('/home');

        $project = Project::latest()->first();

        $this->assertDatabaseHas('projects', $project->toArray());
    }
}
